import hashlib

import requests


class SugarClientError(Exception):

    def __init__(self, status_code, data):
        super(SugarClientError, self).__init__('{}: {}: {}'.format(
            status_code, data['error'], data['error_message']))
        self.status_code = status_code
        self.error = data['error']
        self.error_message = data['error_message']

    def is_not_found(self):
        return self.status_code == 404

    def is_invalid_session(self):
        # Need to use refresh_token
        return self.status_code == 401

    def is_invalid_tokens(self):
        # The refresh_token has expired. Ask to re-login
        return self.status_code == 400

    def __repr__(self):
        return '<{0}>: {1} {2} ({3})'.format(
            'SugarClientError', self.status_code, self.error, self.error_message)


class SugarServerError(Exception):
    def __init__(self, status_code, body):
        super(SugarServerError, self).__init__('{}: {}'.format(status_code, body))
        self.status_code = status_code
        self.error_message = body


class SugarApi(object):

    SUGAR_REST_V10 = 'rest/v10'
    PLATFORM = 'base'

    def __init__(self, server_url, username,
                 client_id, client_secret,
                 password=None, access_token=None,
                 refresh_token=None, timeout=90, platform='base'):
        self._server_url = self._build_server_url(server_url)
        self._username = username
        self._password = password
        self._timeout = timeout
        self._client_id = client_id
        self._client_secret = client_secret
        self._access_token = access_token
        self._refresh_token = refresh_token
        self._credentials = None
        self._user_id = None
        self._current_user_data = None
        self._platform = platform

    def _build_server_url(self, url):
        if url.endswith("/"):
            return url + self.SUGAR_REST_V10
        else:
            return url + '/' + self.SUGAR_REST_V10

    def _build_headers(self):
        headers = {
            "Content-Type": "application/json",
            "OAuth-Token": self.access_token or self._access_token,
        }
        return headers

    def _password_encode(self, password):
        """ Returns md5 hash to send as a password """
        return hashlib.md5(password).hexdigest()

    def _request(self, method, path, json=None, headers=None, params=None, **kwargs):
        url = self._server_url + path
        base_headers = self._build_headers()
        if headers is not None:
            base_headers.update(headers)
        resp = requests.request(method, url, json=json, headers=base_headers,
                                params=params, **kwargs)
        if resp.status_code == 200:
            return resp.json()
        else:
            try:
                error_data = resp.json()
            except ValueError:
                # server failed to returned valid json
                # probably a critical error on the server happened
                raise SugarServerError(resp.status_code, resp.content)
            else:
                raise SugarClientError(resp.status_code, error_data)

    def me(self):
        """ Return details of authenticated user """
        if self._current_user_data is not None:
            return self._current_user_data
        path = '/me'
        data = self._request('get', path)
        self._current_user_data = data['current_user']
        return self._current_user_data

    @property
    def access_token(self):
        if self._credentials:
            return self._credentials['access_token']
        return self._access_token

    @property
    def refresh_token(self):
        if self._credentials:
            return self._credentials['refresh_token']
        return self._refresh_token

    def login(self):
        path = '/oauth2/token'

        params = {
            "grant_type": "password",
            "client_id": self._client_id,
            "client_secret": self._client_secret,
            "username": self._username,
            "password": self._password,
            "platform": self._platform
        }

        self._credentials = self._request('post', path, json=params)
        return self.me

    def refresh(self):
        path = '/oauth2/token'

        params = {
            "grant_type": "refresh_token",
            "client_id": self._client_id,
            "client_secret": self._client_secret,
            "refresh_token": self.refresh_token,
            "platform": self._platform
        }

        self._credentials = self._request('post', path, json=params)
        return self._credentials

    def recent(self):
        path = '/recent'
        params = {
            "module_list": "Accounts,Contacts,Leads,Opportunities,Bugs,Cases",
        }
        return self._request('get', path, params=params)

    def search(self, keyword, max_num=10):
        path = '/search'
        params = {
            'q': keyword,
            'max_num': max_num,
        }
        return self._request('get', path, params=params)

    def get_record(self, module_name, entry_id):
        module_name = module_name.strip().title()
        entry_id = entry_id.strip()
        path = '/{m}/{id}'.format(m=module_name, id=entry_id)
        return self._request('get', path)

    def filter_record_by_email(self, module_name, email):
        module_name = module_name.strip().title()
        path = '/{m}/filter'.format(m=module_name)
        params = {
            'filter[0][email_addresses.email_address][$equals]': email,
        }
        return self._request('get', path, params=params)

    def filter_record(self, module_name, filter):
        module_name = module_name.strip().title()
        path = '/{m}/filter'.format(m=module_name)

        return self._request('get', path, params=filter)

    def create_record(self, module_name, data):
        module_name = module_name.strip().title()
        path = '/{m}'.format(m=module_name)
        return self._request('post', path, json=data)

    def update_record(self, module_name, entry_id, data):
        module_name = module_name.strip().title()
        entry_id = entry_id.strip()
        path = '/{m}/{id}'.format(m=module_name, id=entry_id)

        return self._request('put', path, json=data)

    def relate_record(self, module_name, entry_id, data):
        module_name = module_name.strip().title()
        entry_id = entry_id.strip()
        path = '/{m}/{id}/link'.format(m=module_name, id=entry_id)

        return self._request('post', path, json=data)
