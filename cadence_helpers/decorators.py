from functools import wraps
from datetime import timedelta

import jwt
from flask import request, current_app, g
from flask_restful import abort


def validate_jwt_token(request, *args, **kwargs):
    """
    Validates JWT token format and authorization

    :param request: A flask.request object
    :type request: flask.request

    :return res: dict

    Sample res output
    {
      u'username': u'ronnie', u'user_id': 1, u'exp': 1467877546, u'orig_iat': 1467791146,
      u'email': u'ronnie@collabspot.com', u'tenant': u'collabspot'
    }

    """
    if 'tenant' not in kwargs.keys():
        abort(400, message="tenant not in request url.")

    res = None

    auth_header_value = request.headers.get('Authorization', None)

    if auth_header_value is None:
        abort(403, message="Missing JWT token in request.")

    parts = auth_header_value.split()

    if len(parts) != 2:
        abort(400, message="Malformed JWT token.")

    if parts[0].title() != "Bearer":
        abort(400, message="Token type is not Bearer.")

    tenants = None
    try:
        res = jwt.decode(
            parts[1],
            current_app.config["JWT_PUBLIC_KEY"],
            algorithms=['RS256'],
            leeway=timedelta(seconds=30),
        )

        if isinstance(res["tenant"], (str, unicode)):
            tenants = [res["tenant"]]
        elif isinstance(res["tenant"], list):
            tenants = res["tenant"]
        else:
            tenants = []

        if kwargs["tenant"] not in tenants:
            abort(403, message="You\'re not allowed to access this resource.")

    except jwt.ExpiredSignatureError:
        abort(403, message="JWT token has expired.")
    except jwt.DecodeError:
        abort(400, message="JWT token is malformed.")

    return res


def jwt_required(fn):
    """
    View decorator that requires a valid JWT token to be present in the request
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        res = validate_jwt_token(request, *args, **kwargs)
        # Store decoded JWT token in flask.g
        g.jwt = res
        return fn(*args, **kwargs)
    return wrapper
