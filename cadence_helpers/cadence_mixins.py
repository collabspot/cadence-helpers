from flask import current_app
from flask_restful import abort

from .cadence_api import CadenceApi
from .cadence_api import CadenceClientError


class CadenceApiMixin(object):
    """
    Call this method in the view's method.
    e.g.

    from flask import request
    class MyView(CadenceApiMixin, Resource):
        def get(self):
            self.initialize_cadence_client(request)
    """

    @classmethod
    def parse_auth_header(self, request):
        auth_header_value = request.headers.get('Authorization', None)
        if auth_header_value is None:
            abort(401, "Missing JWT token in the request")
        prefix, token = auth_header_value.split()
        if prefix.title() != "Bearer":
            abort(401, "Token header type is not Bearer")
        cadence_host = current_app.config["CADENCE_HOST"].format(
            tenant=request.view_args["tenant"])
        return cadence_host, token

    def initialize_cadence_client(self, request):
        cadence_host, jwt_token = CadenceApiMixin.parse_auth_header(request)
        self.cadence = CadenceApi(cadence_host, jwt_token)

    def perform_cadence_method(self, func, *args, **kwargs):
        # TODO: We cant refresh JWT token right now since
        # jwt content does not have username and password to
        # call refresh token api.
        resp = None
        try:
            resp = func(*args, **kwargs)
        except CadenceClientError as e:
            abort(e.status_code, message=e.error_message)
        except:
            raise
        return resp
