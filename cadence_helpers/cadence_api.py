import requests


class CadenceClientError(Exception):

    def __init__(self, status_code, data):
        super(CadenceClientError, self).__init__('{}: {}'.format(
            status_code, data))
        self.status_code = status_code
        self.error_message = data

    def is_not_found(self):
        return self.status_code == 404

    def is_invalid_session(self):
        return self.status_code == 401

    def is_invalid_tokens(self):
        return self.status_code == 400

    def __repr__(self):
        return '<{0}>: {1} {2}'.format(
            'CadenceClientError', self.status_code, self.error_message)


class CadenceServerError(Exception):
    def __init__(self, status_code, body):
        super(CadenceServerError, self).__init__('{}: {}'.format(status_code, body))
        self.status_code = status_code
        self.error_message = body


class CadenceApi(object):
    """
    A simple api wrapper for Cadence
    """

    def __init__(self, cadence_host, jwt_token):
        self.cadence_host = cadence_host
        self.jwt_token = jwt_token

    def _build_headers(self):
        return {
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(self.jwt_token)
        }

    def _request(self, method, path, json=None, headers=None, params=None, **kwargs):
        url = self.cadence_host + path
        base_headers = self._build_headers()
        if headers is not None:
            base_headers.update(headers)
        resp = requests.request(method, url, json=json, headers=base_headers,
                                params=params, **kwargs)
        # print "***_request()****"
        # print resp.status_code
        # print resp.text
        return resp

    def get_prospect(self, prospect_id):
        path = "{0}/{1}".format("/api/private/prospects", prospect_id)
        return self._request("GET", path)

    def create_prospect(self, data):
        path = "/api/private/prospects"
        return self._request("POST", path, json=data)

    def get_messages(self, prospect_id, is_sent=None):
        path = "/api/private/messages"
        params = {
            "prospect_id": prospect_id,
        }
        if is_sent:
            params.update({
                "is_sent": "1",
            })
        return self._request("GET", path, params=params)

    def get_calls(self, prospect_id, is_completed=None):
        path = "/api/private/responses"
        params = {
            "prospect_id": prospect_id,
        }
        if is_completed:
            # replace to is_completed in cadence filter
            params.update({
                "completed_calls": "1",
            })
        return self._request("GET", path, params=params)
