from setuptools import setup

setup(name='Cadence Helpers',
      version='0.1',
      description='Assorted classes and methods for developing on Cadence',
      url='http://bitbucket.com/collabspot/cadence-helpers',
      author='Collabspot',
      author_email='rbbeltran.09@gmail.com',
      license='MIT',
      packages=['cadence_helpers'],
      install_requires=[
          'requests==2.10.0',
          'PyJWT==1.4.0',
          'cryptography==1.4',
      ],
      zip_safe=False)
